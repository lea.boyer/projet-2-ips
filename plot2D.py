#!/usr/bin/env python
import numpy as np
import matplotlib.pyplot as plt

values = np.loadtxt(open("densite.csv", "rb"), delimiter=",")
x=np.linspace(-5,5,100)
plt.matshow(values, extent = [-20, 20, -10, 10])
plt.ylabel('Dimension r')
plt.xlabel("Dimension z")
plt.title("Densite locale d'un systeme nucleaire")
plt.colorbar()
plt.savefig("densite2D.png")