#include <iostream>
#include <armadillo>

/** Classe permettant de calculer les polynomes de Hermite et  Laguerre.
*@param: hermiteVal résultat de la récurrance sur Hermite.
*@param: laguerreVal résultat de la récurrence sur Hermite
*/
class Poly {
public:
    arma::mat hermiteVal;
    arma::cube laguerreVal;
    void calcHermite(int, arma::vec);
    void reccurrenceHermite(arma::vec, arma::vec, arma::vec, int, int );
    arma::vec hermite(int);
    void calcLaguerre(int, int, arma::vec);
    void reccurrenceLaguerre(arma::vec, arma::vec, int,int,int );
    void reccurrenceLaguerre2(arma::vec, arma::vec, arma::vec, int,int,int );
    arma::vec laguerre(int,int);

};