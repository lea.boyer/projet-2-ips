#include <armadillo>
#include <iostream>
#include <math.h>
#include "Poly.h"

/** Classe permettant de calculer la troncature de la base.
*@param: _br Paramètre de déformation de la base
*@param: _bz Paramètre de déformation de la base
*@param: _N Paramètre de troncature de la base
*@param: _Q Paramètre de troncature de la base
*@param: mMax Maximum de nombre quantique m
*@param: nMax Maximum de nombre quantique n
*@param: n_zMax Maximum de nombre quantique n_z
*/
class Basis {

public:
    int N = 14;
    double Q = 1.3;
    double br = 1.935801664793151;
    double bz = 2.829683956491218;
    int mMax = 14;
    arma::ivec nMax;
    arma::imat n_zMax;
    void set_nMax();//dépend de m
    void set_n_zMax();//dépend de n et m
    arma::ivec get_nMax();
    arma::imat get_n_zMax();
    long int fact(int);
    arma::vec rPart(arma::vec,int,int);
    arma::vec zPart(arma::vec,int);
    arma::mat basisFunc(int, int, int, arma::vec, arma::vec);

    void setM();
    int getM();
    void setN();
    int getN();

    Basis(double,double,int,double);//Basis::Basis(br,bz,N,Q)
    Basis();
};
