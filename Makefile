CC = g++
CFLAGS = -Wall -Wextra -O3 -std=c++11 -Iheaders
LIBS = -larmadillo
TARGET = main testBasis testPoly
OBJS = obj/Main.o obj/Density.o obj/Basis.o obj/Poly.o obj/TestBasis.o obj/TestPoly.o 
all: $(TARGET)

main: obj/Main.o obj/Poly.o obj/Density.o obj/Basis.o
	$(CC) $(CFLAGS) $^ -o $@

obj/Main.o: src/Main.cpp obj/Poly.o obj/Density.o obj/Basis.o
	$(CC) $(CFLAGS) $< -c -o $@

testBasis: obj/TestBasis.o obj/Basis.o obj/Poly.o
	$(CC) $(CFLAGS) $^ -o $@
	
testPoly: obj/TestPoly.o  obj/Poly.o
	$(CC) $(CFLAGS) $^ -o $@

obj/TestBasis.o: src/TestBasis.cpp headers/TestBasis.h
	$(CC) $(CFLAGS) $< -c -o $@

obj/TestPoly.o: src/TestPoly.cpp headers/TestPoly.h
	$(CC) $(CFLAGS) $< -c -o $@

obj/Density.o: src/Density.cpp  headers/Density.h obj/Poly.o obj/Basis.o
	$(CC) $(CFLAGS) $< -c -o $@

obj/Basis.o: src/Basis.cpp  headers/Basis.h obj/Poly.o
	$(CC) $(CFLAGS) $< -c -o $@

obj/Poly.o: src/Poly.cpp headers/Poly.h
	$(CC) $(CFLAGS) $< -c -o $@

src/TestBasis.cpp: headers/TestBasis.h
	cxxtestgen --error-printer -o src/TestBasis.cpp  headers/TestBasis.h cxxtest/TestSuite.h

src/TestPoly.cpp: headers/testPoly.h
	cxxtestgen --error-printer -o src/testPoly.cpp  headers/testPoly.h cxxtest/TestSuite.h

clean:
	rm -f $(OBJS)
	rm -f $(TARGET)










