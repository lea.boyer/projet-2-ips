#include <iostream>
#include <string>
using namespace std;
#include "Density.h"
#include "Basis.h"

Density::Density() {
}


/** Algo natif sans optimisation
*@param: zVals Vecteur z
*@param: rVals Vecteur r
*@return matrice de densité
*/
arma::mat Density::nativeAlgo(arma::vec zVals, arma::vec rVals) {
    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);
    arma::wall_clock timer;
    timer.tic();
    Basis basis;
    int a=0;
    int b=0;

    arma::mat result = arma::zeros(zVals.size(), rVals.size()); // number of points on r- and z- axes
    for (int m = 0; m < basis.mMax; m++) {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {
                            arma::mat funcA = basis.basisFunc( m,  n,  n_z, zVals, rVals);
                            arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
                            result += funcA % funcB * rho(a,b) ; // mat += mat % mat * double
                            a++;
                        }
                    }
                }
                a=0;
                b++;
            }
        }
    }
    double t= timer.toc();
    printf(" temps: %f\n",t);
    return result;

}

/** On fait le calcul de funcA plus haut dans les boucles pour éviter des calculs superflus
*@param: zVals Vecteur z
*@param: rVals Vecteur r
*@return matrice de densité
*/
arma::mat Density::Algov2(arma::vec zVals, arma::vec rVals) {
    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);
    arma::wall_clock timer;
    timer.tic();
    Basis basis;
    int a=0;
    int b=0;

    arma::mat result = arma::zeros(zVals.size(), rVals.size()); // number of points on r- and z- axes
    for (int m = 0; m < basis.mMax; m++) {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {   
                arma::mat funcA = basis.basisFunc( m,  n,  n_z, zVals, rVals);
                
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {
                            arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
                            result += funcA % funcB * rho(a,b) ; // mat += mat % mat * double
                            a++;
                        }
                    }
                }
                a=0;
                b++;
            }
        }
    }
    double t= timer.toc();
    printf(" temps: %f\n",t);
    return result;

}

