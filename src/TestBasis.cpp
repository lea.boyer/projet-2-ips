/* Generated file, do not edit */

#ifndef CXXTEST_RUNNING
#define CXXTEST_RUNNING
#endif

#define _CXXTEST_HAVE_STD
#include <cxxtest/TestListener.h>
#include <cxxtest/TestTracker.h>
#include <cxxtest/TestRunner.h>
#include <cxxtest/RealDescriptions.h>
#include <cxxtest/TestMain.h>
#include <cxxtest/ErrorPrinter.h>

int main( int argc, char *argv[] ) {
 int status;
    CxxTest::ErrorPrinter tmp;
    CxxTest::RealWorldDescription::_worldName = "cxxtest";
    status = CxxTest::Main< CxxTest::ErrorPrinter >( tmp, argc, argv );
    return status;
}
bool suite_TestBasis_init = false;
#include "/mnt/c/git/projet-2-ips/headers/TestBasis.h"

static TestBasis suite_TestBasis;

static CxxTest::List Tests_TestBasis = { 0, 0 };
CxxTest::StaticSuiteDescription suiteDescription_TestBasis( "headers/TestBasis.h", 4, "TestBasis", suite_TestBasis, Tests_TestBasis );

static class TestDescription_suite_TestBasis_testBasis : public CxxTest::RealTestDescription {
public:
 TestDescription_suite_TestBasis_testBasis() : CxxTest::RealTestDescription( Tests_TestBasis, suiteDescription_TestBasis, 7, "testBasis" ) {}
 void runTest() { suite_TestBasis.testBasis(); }
} testDescription_suite_TestBasis_testBasis;

#include <cxxtest/Root.cpp>
const char* CxxTest::RealWorldDescription::_worldName = "cxxtest";
