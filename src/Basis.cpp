#include "Basis.h"
#include <armadillo>


/** Constructeur de la classe Basis.
*@param: _br Paramètre de déformation de la base
*@param: _bz Paramètre de déformation de la base
*@param: _N Paramètre de troncature de la base
*@param: _Q Paramètre de troncature de la base
*/
Basis::Basis(double _br, double _bz, int _N, double _Q)
{
    br= _br;
    bz=_bz;
    N=_N;
    Q=_Q;
    set_nMax();
    set_n_zMax();
}

/** Calcule le nMax de la troncature de la base
*@return : Renvoie nMax.
*/
void Basis::set_nMax()
{
    nMax = arma::ivec(mMax);
    for (int m = 0; m < mMax; m++)
    {
        nMax(m) = 0.5*(mMax - m - 1) + 1;
    }

}

/** Calcule le n_zMax de la troncature de la base
*@return : Renvoie n_zMax.
*/
void Basis::set_n_zMax()
{
    n_zMax = arma::imat(mMax,nMax(0), arma::fill::zeros); //Initialisation avec nb_row = mMax et nb_col = max(nMax) donc nMax(0)
    for(int m = 0; m < mMax;  m++)
    {
        for (int n = 0; n < nMax(m); n++)
        {
            double exponent = 2./3.;
            n_zMax(m,n) = (N+2) * pow(Q,exponent) + 0.5 - Q * (m + 2*n + 1) ;
        }

    }
}

/** Fonction calculant la factorielle d'un nombre.
*@param n : Nombre sur lequel appliquer la factorielle.
*@return : Résultat de la factorielle.
*/
long int Basis::fact(int max)
{

    long int resultat=1;
    for (int m=1; m<=max; m++) {
        resultat = resultat *  m;
    }
    return resultat;
}

/** Calculateur de la r partie
*@param: r
*@param: m : maximum du m
*@param: n : maximum du n
*@return : la r partie de la fonction de base.
*/
arma::vec Basis::rPart(arma::vec r, int m,int n) {
    Poly poly;
    poly.calcLaguerre(m+1,n+1, (r%r / (br*br)));
    arma::vec exp = arma::exp(- r % r / (2 * br * br));
    return 1./(double)(br * sqrt((double)arma::datum::pi)) * sqrt( (double)fact(n) / (double)fact(n + m)) * exp % arma::pow((r/br),m) % poly.laguerre(m,n);
}

/** Calculateur de la z partie
*@param: z
*@param: n : maximum du n
*@return : la z partie de la fonction de base.
*/
arma::vec Basis::zPart(arma::vec z, int n) {
    Poly poly;
    poly.calcHermite(n+1,(z/bz));
    return (1./(double)sqrt(bz*pow(2,n)* (double)sqrt(arma::datum::pi)*fact(n)))* exp(- ((z%z)/(2*bz*bz))) % poly.hermite(n);

}

/** Fonction de la base
*@param: m
*@param: n
*@param: n_z
*@param: z
*@param: r
*@return : Matrice de base
*/
arma::mat Basis::basisFunc(int m, int n, int n_z, arma::vec z, arma::vec r) {
    return zPart(z, n_z) * rPart(r,m,n).t();
}

/** Accesseur nMax
*@return : Renvoie nMax.
*/
arma::ivec Basis::get_nMax()
{
    return nMax;
}

/** Accesseur n_zMax
*@return : Renvoie n_zMax.
*/
arma::imat Basis::get_n_zMax()
{
    return n_zMax;
}

Basis::Basis()
{
    set_nMax();
    set_n_zMax();

}
