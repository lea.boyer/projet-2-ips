#include "Poly.h"

/** Méthode principale du calcul du polynome Hermite. Instancie le polynome pour n=0 et n=1.
*@param nMax Incrémentation maximale de n.
*@param zVals Variable représentant un axe à une dimension.
*/
void Poly::calcHermite(int nMax, arma::vec zVals) {
    arma::vec Hn;
    Hn.set_size(zVals.size());
    Hn.fill(1);
    arma::vec Hn2 =  2* zVals;
    hermiteVal = arma::mat(zVals.size(), nMax);
    hermiteVal.col(0)= Hn;
    if(nMax>1) hermiteVal.col(1)= Hn2;
    if(nMax>2)reccurrenceHermite(Hn, Hn2, zVals, 2, nMax);

}

/** Méthode calculant la récurrence de Hermite.
*@param Hn Valeur de H(n-1).
*@param Hn2 : valeur de H(n).
*@param n : n de départ pour la récurrence.
*@param nmax : valeur de n maximale.
*@param z Variable représentant un axe à une dimension.
*@return  Renvoie H(n).
*/
void Poly::reccurrenceHermite(arma::vec Hn, arma::vec Hn2, arma::vec z, int n, int nmax) {
    if(n<nmax) {
        arma::vec Hn3= 2 * z % Hn2 - 2 *(n-1)* Hn;
        Hn= Hn2;
        Hn2= Hn3;
        hermiteVal.col(n)= Hn2;
        reccurrenceHermite(Hn, Hn2, z,n+1, nmax);
    }
}

/** Accesseur de la nième valeur de hermite.
*@param n indice de la valeur à renvoyer.
*@return La nième valeur de hermiteVal.
*/
arma::vec Poly::hermite(int n) {
    return hermiteVal.col(n);
}

/** Méthode principale du calcul du polynome Laguerre.
*@param mMax Incrémentation maximale de m.
*@param nMax Incrémentation maximale de n.
*@param zVals Variable représentant un axe à une dimension.
*/
void Poly::calcLaguerre(int mMax, int nMax,  arma::vec zVals) {
    arma::vec Hn;
    Hn.set_size(zVals.size());
    Hn.fill(1);
    laguerreVal= arma::cube( zVals.size(), nMax+1, mMax+1);
    laguerreVal.slice(0).col(0)= Hn;
    reccurrenceLaguerre(Hn, zVals, nMax, 0, mMax);


}

/** Méthode calculant la récurrence de Laguerre sur m.
*@param Hn Valeur de H(0).
*@param z Variable représentant un axe à une dimension.
*@param m m de départ pour la récurrence.
*@param mmax valeur de m maximale.
*@return  Renvoie une matrice de H(m,n).
*/
void Poly::reccurrenceLaguerre(arma::vec Hn, arma::vec z, int nmax, int m, int mmax) {
    if((m<mmax)) {
        arma::vec Hn2= 1 + m - z;
        laguerreVal.slice(m).col(0)= Hn;
        laguerreVal.slice(m).col(1)= Hn2;
        reccurrenceLaguerre2(Hn, Hn2, z, 2, nmax,m);
        m++;
        reccurrenceLaguerre(Hn, z, nmax,m, mmax);
    }
}

/** Méthode calculant la récurrence de Laguerre sur m.
*@param Hn Valeur de H(0).
*@param Hn2 Valeur de H(1).
*@param z Variable représentant un axe à une dimension.
*@param n n de départ pour la récurrence.
*@param nmax valeur de m maximale.
*@param m valeur de m .
*@return Renvoie un vecteur de H(n).
*/
void Poly::reccurrenceLaguerre2(arma::vec Hn, arma::vec Hn2, arma::vec z, int n, int nmax, int m) {
    if(n<nmax ) {
        arma::vec Hn3= (2.0 + ((double)m -1.0 - z)/n) % Hn2 - (1.0 + (m-1)/(double)n)*Hn;
        Hn= Hn2;
        Hn2= Hn3;
        laguerreVal.slice(m).col(n)= Hn2;
        reccurrenceLaguerre2(Hn, Hn2, z, (n+1), nmax, m);
    }
}

/** Accesseur de la nième valeur de laguerre.
*@param mn indice de la valeur à renvoyer.
*@return La mième et nième valeur de hermiteVal.
*/
arma::vec Poly::laguerre(int m, int n) {
    return laguerreVal.slice(m).col(n);
}
