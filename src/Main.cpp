#include <iostream>
#include <fstream>
#include <string>
using namespace std;
#include "Density.h"

arma::cube cylinder_to_cartesian(arma::mat matrice) //matrice = la matrice qu'on sort de NaiveAlgo / La fonction sert à créer une matrice en 3 dimension représentant la valeur de la densité selon les axes x,y et z.
{
    int nbp_r = matrice.n_cols;
    int nbp_z = matrice.n_rows;
    int x_min = -10, x_max = 10, nbp_x = 32;
    int y_min = -10, y_max = 10, nbp_y = 32;

    arma::cube cartesian = arma::cube(nbp_x, nbp_y, nbp_z).zeros();
    for (int ix = 0; ix < nbp_x; ix++) {
        for (int iy = 0; iy < nbp_y; iy++) {
            double x = x_min + ((double) ix/nbp_x)*(x_max - x_min);
            double y = y_min + ((double) iy/nbp_y)*(y_max - y_min);
            double r = sqrt(pow(x, 2) + pow(y, 2));
            int ir = (r/sqrt(x_max*x_max + y_max*y_max)) * nbp_r;
            if (ir < nbp_r)
                //std::cout << x << "\n";
                cartesian.tube(ix, iy) = matrice.col(r);
        }
    }
    return cartesian;
}

std::string cubeToDf3(const arma::cube &m)
{
    std::stringstream ss(std::stringstream::out | std::stringstream::binary);
    int nx = m.n_rows;
    int ny = m.n_cols;
    int nz = m.n_slices;
    ss.put(nx >> 8);
    ss.put(nx & 0xff);
    ss.put(ny >> 8);
    ss.put(ny & 0xff);
    ss.put(nz >> 8);
    ss.put(nz & 0xff);
    double theMin = 0.0;
    double theMax = m.max();
    for (uint k = 0; k < m.n_slices; k++)
    {
        for (uint j = 0; j < m.n_cols; j++)
        {
            for (uint i = 0; i < m.n_rows; i++)
            {
                uint v = 255 * (fabs(m(i, j, k)) - theMin) / (theMax - theMin);
                ss.put(v);
            }
        }
    }
    return ss.str();
}


int main() {
    arma::vec z = arma::linspace(-10, 10, 32);
    arma::vec r = arma::linspace(-20, 20, 64);
    Density dens;
    arma::mat in= dens.nativeAlgo(z,r);
    //in.col(0).print();
    in =dens.Algov2(z,r);
    //in.col(0).print();
    arma::mat out= in.t();
    // in.save("densite.csv", arma::csv_ascii);
    // arma::cube cube = cylinder_to_cartesian(dens.nativeAlgo(z,r).t());
    // std::string s= cubeToDf3(cube);
    // ofstream monFlux("example.df3");
    // if(monFlux)
    //     monFlux << s << endl ;
    // else
    //     cout << "ERREUR: Impossible d'ouvrir le fichier.";
    return 0;
}